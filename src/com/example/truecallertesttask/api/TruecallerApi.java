package com.example.truecallertesttask.api;

import android.util.Log;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * User: Pavel
 * Date: 23.01.15
 * Time: 16:14
 */
public class TruecallerApi {

    private static final Boolean LOG = true;
    private static final String TAG = TruecallerApi.class.getSimpleName();

    public static final String MAIN_URL = "http://truecaller.com";
    public static final int CHAR_NUMBER = 10;
    public static final String WHITE_SPACE_CHARACTER = "(space)";

    public static final String ERROR_CONTENT_TOO_SHORT = "Content of truecaller website shorter than " + CHAR_NUMBER + " symbols";
    public static final String ERROR_RESPONSE_UNSUCCESSFUL = "Response unsuccessful";
    public static final String ERROR_UNEXPECTED_CODE = "Unexpected code";

    private Request request;

    public TruecallerApi() {
        request = new Request.Builder().url(MAIN_URL).build();
    }

    public void get10thCharacter(final Callback callback) {
        if (LOG) Log.e(TAG, "get10thCharacter");

        new OkHttpClient().newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                callback.onFail(ERROR_RESPONSE_UNSUCCESSFUL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    callback.onFail(ERROR_UNEXPECTED_CODE);
                    throw new IOException("Unexpected code " + response);
                } else {
                    String s = response.body().string();
                    if (s.length() > CHAR_NUMBER) {
                        String mySymbol = s.substring(CHAR_NUMBER - 1, CHAR_NUMBER);
                        if (!Character.isWhitespace(mySymbol.toCharArray()[0])) {
                            callback.onSuccess(s.substring(CHAR_NUMBER - 1, CHAR_NUMBER));
                        } else {
                            callback.onSuccess(WHITE_SPACE_CHARACTER);
                        }

                    } else {
                        callback.onFail(ERROR_CONTENT_TOO_SHORT);
                    }
                }
            }
        });

    }

    public void getEvery10thCharacter(final Callback callback) {
        if (LOG) Log.e(TAG, "getEvery10thCharacter");

        (new OkHttpClient()).newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                callback.onFail(ERROR_RESPONSE_UNSUCCESSFUL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    callback.onFail(ERROR_UNEXPECTED_CODE);
                    throw new IOException("Unexpected code " + response);
                } else {
                    String s = response.body().string();
                    if (s.length() > CHAR_NUMBER) {
                        callback.onSuccess(getEvery10thCharacterOfString(s));
                    } else {
                        callback.onFail(ERROR_CONTENT_TOO_SHORT);
                    }
                }
            }
        });
    }

    public void getWordCount(final String word, final Callback callback) {
        if (LOG) Log.e(TAG, "getWordCount");

        new OkHttpClient().newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                callback.onFail(ERROR_RESPONSE_UNSUCCESSFUL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    callback.onFail(ERROR_UNEXPECTED_CODE);
                    throw new IOException("Unexpected code " + response);
                } else {
                    int count = 0;
                    for (String s : response.body().string().split(" ")) {
                        if (s.toLowerCase().equals(word.toLowerCase())) {
                            count++;
                        }
                    }
                    callback.onSuccess(String.valueOf(count));
                }
            }
        });
    }

    private String getEvery10thCharacterOfString(String s) {

        String result = "";

        for (int i = 0; i < s.toCharArray().length; i++) {
            if ((i + 1) % 10 == 0) {
                if (!Character.isWhitespace(s.toCharArray()[i])) {
                    result = result + s.toCharArray()[i] + ", ";
                } else {
                    result = result + WHITE_SPACE_CHARACTER + ", ";
                }
            }
        }

        return result;

    }

    public interface Callback {

        public void onFail(String s);

        public void onSuccess(String s);

    }

}
