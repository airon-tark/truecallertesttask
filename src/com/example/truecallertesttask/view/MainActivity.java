package com.example.truecallertesttask.view;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.example.truecallertesttask.R;
import com.example.truecallertesttask.api.TruecallerApi;

public class MainActivity extends Activity {

    public static final boolean LOG = true;
    public static final String TAG = MainActivity.class.getSimpleName();

    private TextView textView1, textView2, textView3;
    private TruecallerApi api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        api = new TruecallerApi();

        textView1 = (TextView) findViewById(R.id.text1);

        textView2 = (TextView) findViewById(R.id.text2);
        textView2.setMovementMethod(new ScrollingMovementMethod());

        textView3 = (TextView) findViewById(R.id.text3);
    }

    public void onStartClicked(View v) {

        api.getEvery10thCharacter(new TruecallerApi.Callback() {
            @Override
            public void onFail(String s) {
                if (LOG) Log.e(TAG, "onFail");
                setTextToTextView(textView2, s);
            }

            @Override
            public void onSuccess(String s) {
                if (LOG) Log.e(TAG, "onSuccess - " + s);
                setTextToTextView(textView2, s);
            }
        });

        api.getWordCount("Truecaller", new TruecallerApi.Callback() {
            @Override
            public void onFail(String s) {
                if (LOG) Log.e(TAG, "onFail");
                setTextToTextView(textView3, s);
            }

            @Override
            public void onSuccess(String s) {
                if (LOG) Log.e(TAG, "onSuccess - " + s);
                setTextToTextView(textView3, s);
            }
        });

        api.get10thCharacter(new TruecallerApi.Callback() {
            @Override
            public void onFail(String s) {
                if (LOG) Log.e(TAG, "onFail");
                setTextToTextView(textView1, s);
            }

            @Override
            public void onSuccess(String s) {
                if (LOG) Log.e(TAG, "onSuccess - " + s);
                setTextToTextView(textView1, s);
            }
        });

    }

    private void setTextToTextView(final TextView textView, final String text) {
        if (LOG) Log.e(TAG, "setTextToTextView");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setText(text);
            }
        });
    }

}
